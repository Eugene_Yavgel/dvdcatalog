<div class="dvds view">
    <h2><?php echo h($dvd['Dvd']['name']); ?></h2>
    <img src="/dvdcatalog/images/view/100/*/true/<?php echo $dvd['Dvd']['image']; ?>" alt="DVD Image" />
    <dl>
    	<dt>Name:</dt>
        <dd><?php echo h($dvd['Dvd']['name']); ?></dd>	

        <dt>Format:</dt>
        <dd><?php echo h($dvd['Format']['name']); ?></dd>
        
        <dt>Type:</dt>
        <dd><?php echo h($dvd['Type']['name']); ?></dd>
        
        <dt>Location:</dt>
        <dd><?php echo h($dvd['Location']['name']); ?></dd>
		
	<dt>Rating:</dt>
        <dd><?php echo $dvd['Dvd']['rating']; ?></dd>
        
        <dt>Genres:</dt>
	<dd><?php echo $dvd['Dvd']['genres']; ?></dd>
    </dl>    
    
    <p><ul class="actions">
    	<li><?php echo $this->Html->link('List DVDs', array('action'=>'index'));?></li>
    </ul></p>
</div>