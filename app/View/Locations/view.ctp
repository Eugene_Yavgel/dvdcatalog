<div class="locations view">
    <h2>Viewing Location: <?php echo h($location['Location']['name']); ?></h2>
    
    <dl>
    	<dt>Name:</dt>
        <dd><?php echo h($location['Location']['name']); ?></dd>
        
        <dt>Description:</dt>
        <dd><?php echo h($location['Location']['descr']); ?></dd>
        
        <dt>Created</dt>
        <dd><?php echo $location['Location']['created']; ?></dd>
    </dl>
    
    <?php if(!empty($location['Dvd'])): ?>
    
    <div class="related">
        <h3>DVDs with this Location</h3>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($location['Dvd'] as $dvd): ?>
                <tr>
                    <td><?php echo h($dvd['name']); ?></td>
                    <td><?php echo $this->Html->link('View', '/dvds/view/'.$dvd['slug']);?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
    <?php endif; ?>
    
    <p><ul class="actions">
    	<li><?php echo $this->Html->link('List Locations', array('action'=>'index'));?></li>
    </ul></p>
</div>