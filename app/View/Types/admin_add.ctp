<div class="types form">
    <?php echo $this->Form->create('Type'); ?>
        <fieldset>
            <legend>Add a Type</legend>
            <?php
                // create the form inputs
                echo $this->Form->input('name', array('label'=>'Name:'));
                echo $this->Form->input('descr', array('label'=>'Description:', 'type'=>'textarea'));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Add');?>
</div>

<ul class="actions">
    <li><?php echo $this->Html->link('List Types', array('action'=>'index'));?></li>
</ul>