<div class="genres view">
    <h2>Viewing Genre: <?php echo h($genre['Genre']['name']); ?></h2>
    
    <p><dl>
    	<dt>Name:</dt>
        <dd><?php echo h($genre['Genre']['name']); ?></dd>
    </dl></p>
    
    
    <div class="related">
        <h3>Related DVDs</h3>
        <table class="stripe">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($genre['Dvd'] as $dvd): ?>
                    <tr>
                        <td><?php echo $dvd['name']; ?></td>
                        <td><?php echo $this->Html->link('View', array('action'=>'view', 'controller'=>'dvds', $dvd['slug']) );?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
    <p><ul class="actions">
    	<li><?php echo $this->Html->link('List Genres', array('action'=>'index'));?></li>
    </ul></p>
</div>