<?php

class UsersController extends AppController {
	
    /* index() - main page for users
     * url: /users/index
     */
    public function index() {
        // redirect to login page
        $this->redirect('login');
    }

    /* login() - main index page for login
     * url: /users/login
     */
    public function login() {
        // if the form has been submitted
        if(!empty($this->request->data)) {
            // check the username and password
            if( ($user = $this->User->check_login($this->request->data)) ) {
                // save the user information to the session
                $this->Session->write('User', $user);
                // set flash messsage
                $this->Session->setFlash('You have successfully logged in.', 'default', array(), 'flash_good');
                // redirect the user
                $this->redirect('/dvds/admin_index/');
            } else {
                // set error message
                $this->set('error', 'ERROR: Invalid Username or Password.');
            }
        }
    }

    /* logout() - logs out a user
     * url: /users/logout
     */
    public function logout() {
        // delete the User session
        $this->Session->delete('User');
        // set flash message
        $this->Session->setFlash('You have successfully logged out.', 'default', array(), 'flash_good');
            // redirect the user
        $this->redirect(array('action'=>'login', 'controller'=>'users')); 
    }
    
    
}
?>