<div class="formats view">
    <h2>Viewing Format: <?php echo h($format['Format']['name']); ?></h2>
    
    <dl>
    	<dt>Name:</dt>
        <dd><?php echo h($format['Format']['name']); ?></dd>
        
        <dt>Description:</dt>
        <dd><?php echo h($format['Format']['descr']); ?></dd>
        
        <dt>Created</dt>
        <dd><?php echo $format['Format']['created']; ?></dd>
    </dl>
    
    <?php if(!empty($format['Dvd'])): ?>
    
    <div class="related">
        <h3>DVDs with this Format</h3>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($format['Dvd'] as $dvd): ?>
                <tr>
                    <td><?php echo h($dvd['name']); ?></td>
                    <td><?php echo $this->Html->link(h('View', '/dvds/view/'.$dvd['slug']));?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
    <?php endif; ?>
    
    <p><ul class="actions">
    	<li><?php echo $this->Html->link('List Formats', array('action'=>'index'));?></li>
    </ul></p>
</div>