<div class="genres index">
    <h2>Genres Admin Index</h2>
    <p>Currently displaying all Genres in the application</p>

    <?php
	// check $genres variable exists and is not empty
	if(isset($genres) && !empty($genres)):
    ?>

    <table class="stripe">
    	<thead>
            <tr>
		<th>Name</th>
		<th>Created</th>
		<th>Modified</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($genres as $genre): ?>
                <tr>
                    <td><?php echo $genre['Genre']['name']; ?></td>
                    <td><?php echo $genre['Genre']['created']; ?></td>
                    <td><?php echo $genre['Genre']['modified']; ?></td>
                    <td>
			<?php echo $this->Html->link('Edit', array('action'=>'admin_edit', $genre['Genre']['id']) );?>
                        <?php echo $this->Html->link('Delete', array('action'=>'admin_delete', $genre['Genre']['id']), null, sprintf('Are you sure you want to delete Genre: %s?', $genre['Genre']['name']));?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?php
	else:
            echo 'There are currently no Genres in the database.';
	endif;
    ?>
    
    <p><ul class="actions">
	<li><?php echo $this->Html->link('Add a Genre', array('action'=>'admin_add')); ?></li>
    </ul></p>
</div>