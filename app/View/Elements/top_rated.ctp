<ol>
    <?php
        // get top rated dvds
        // i'm using the paginator so i can specifiy sql statements here
        $top_rated = $this->requestAction('dvds/footer/sort:rating/direction:desc/limit:5');
        // loop through dvds
        foreach($top_rated as $dvd) {
            echo "<li><a href='/dvdcatalog/dvds/view/".$dvd['Dvd']['slug']."'>".$dvd['Dvd']['name']."</a></li>";
        }
    ?>
</ol>