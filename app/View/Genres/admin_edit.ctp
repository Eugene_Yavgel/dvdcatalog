<div class="genres form">
    <?php echo $this->Form->create('Genre');?>
        <fieldset>
            <legend>Edit a Genre</legend>
            <?php
                // create the form inputs
                echo $this->Form->input('id');
                echo $this->Form->input('name', array('label'=>'Name:'));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Save');?>
</div>

<div class="related">
    <h3>Related DVDs</h3>
    <table class="stripe">
        <thead>
            <tr>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($this->data['Dvd'])): ?>
                <?php foreach($this->data['Dvd'] as $dvd): ?>
                    <tr>
                        <td><?php echo $dvd['name']; ?></td>
                        <td><?php echo $this->Html->link('Edit', array('action'=>'admin_edit','controller'=>'dvds', $dvd['id']) );?></td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="2">There are currently no DVDs to display</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>

<p><ul class="actions">
    <p><li><?php echo $this->Html->link('List Genres', array('action'=>'index'));?></li></p>
    <p><li><?php echo $this->Html->link('Add a Genre', array('action'=>'admin_add'));?></li></p>
</ul></p>