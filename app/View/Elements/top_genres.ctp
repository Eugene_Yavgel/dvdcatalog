<ol>
    <?php
        // get top genres
        $top_genres = $this->requestAction('dvds/top_genres');
        // loop through dvds
        foreach($top_genres as $genre => $number) {
            echo "<li><a href='/dvdcatalog/genres/view/".$genre."'>".ucfirst($genre)."</a></li>";
        }
    ?>
</ol>