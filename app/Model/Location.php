<?php

class Location extends AppModel {
	
    // setup the has many relationships
    public $hasMany = array(
        'Dvd'=>array(
            'className'=>'Dvd'
        )
    );
    
    // setup form validation for locations
    public $validate = array(
	// name field
	'name' => array(
            // must not be empty
            'rule' => 'notEmpty',
            // error message to display
            'message' => 'Please enter a Location Name'
	)
    );
}

?>