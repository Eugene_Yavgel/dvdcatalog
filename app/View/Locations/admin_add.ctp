<div class="locations form">
    <?php echo $this->Form->create('Location'); ?>
        <fieldset>
            <legend>Add a Location</legend>
            <?php
                // create the form inputs
                echo $this->Form->input('name', array('label'=>'Name:'));
                echo $this->Form->input('descr', array('label'=>'Description:', 'type'=>'textarea'));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Add');?>
</div>

<ul class="actions">
    <li><?php echo $this->Html->link('List Locations', array('action'=>'index'));?></li>
</ul>