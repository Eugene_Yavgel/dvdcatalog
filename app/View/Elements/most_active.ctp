<ol>
    <?php
        // get most active dvds
        $most_active = $this->requestAction('dvds/footer/sort:views/direction:desc/limit:5');
        // loop through dvds
        foreach($most_active as $dvd) {
            echo "<li><a href='/dvdcatalog/dvds/view/".$dvd['Dvd']['slug']."'>".$dvd['Dvd']['name']."</a></li>";
        }
    ?>
</ol>