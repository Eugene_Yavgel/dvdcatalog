<div class="genres form">

<?php echo $this->Form->create('Genre');?>
    <fieldset>
        <legend>Add a Genre</legend>
        <?php
            // create the form inputs
            echo $this->Form->input('name', array('label'=>'Name:'));
        ?>
    </fieldset>
<?php echo $this->Form->end('Add');?>
</div>

<p><ul class="actions">
    <li><?php echo $this->Html->link('List Genres', array('action'=>'index'));?></li>
</ul></p>