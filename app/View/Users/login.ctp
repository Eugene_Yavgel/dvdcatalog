<div class="login form">
    <?php if(isset($error)): ?>
        <p class="flash_bad">
            <?php echo $error; ?>
        </p>
    <?php endif; ?>

    <?php echo $this->Form->create('User', array('url' => 'login')); ?>
        <fieldset>
            <legend>Login</legend>
            <div class="input required">
                <label>Username:</label>
                <?php echo $this->Form->text('User.username'); ?>
            </div>
            <div class="input required">
                <label>Password:</label>
                <?php echo $this->Form->password('User.password'); ?>
            </div>
        </fieldset>
    <?php echo $this->Form->end('Login'); ?>
</div>