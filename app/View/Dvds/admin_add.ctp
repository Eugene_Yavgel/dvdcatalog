<div class="dvds form">
    <?php
        // if there was an error uploading the file then display errors here
        if(isset($errors)) {
            echo $this->misc->display_errors($errors);
        }
    ?>

    <?php echo $this->Form->create('Dvd', array('type'=>'file'));?>
        <fieldset>
            <legend>Add a Dvd</legend>
            <?php
                // create the form inputs
                echo $this->Form->input('name', array('label'=>'Name:'));
                echo $this->Form->input('format_id', array('label'=>'Format:', 'type'=>'select', 'options'=>$formats));
                echo $this->Form->input('type_id', array('label'=>'Type:', 'class'=>'type_select'));
                echo $this->Form->input('location_id', array('label'=>'Location:'));
                echo $this->Form->input('File.image', array('label'=>'Image:', 'type'=>'file'));
                echo $this->Form->input('rating', array('label'=>'Rating:'));
                echo $this->Form->input('website', array('label'=>'Website URL:'));
                echo $this->Form->input('imdb', array('label'=>'Imdb URL:'));
                echo $this->Form->input('discs', array('label'=>'Number of Discs:', 'class'=>'tv_hide'));
                echo $this->Form->input('episodes', array('label'=>'Number of Episodes:', 'class'=>'tv_hide'));
                
                echo $this->Form->input('genres', array('label'=>'Genres:', 'type'=>'text'));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Add');?>
</div>

<ul class="actions">
    <p><li><?php echo $this->Html->link('List DVDs', array('action'=>'index'));?></li></p>
</ul>