<div id="wrapper-header">
    <div id="header">
        <div class="logo">
            <h1>CakeCatalog</h1>
            <h2>an online application to track and catalog your collection of dvds built using cakephp</h2>
        </div>

        <div class="filters">
            <form action="/dvdcatalog/dvds" method="post">
                <fieldset>
                    <div class="input">
                        <?php echo $this->Form->input('format', array(
                            'label' => '', 
                            'type' => 'select', 
                            'options' => $formats,
                            'selected' => $this->request->data['format']
                        )); ?>
                    </div>
                    <div class="input">
                        <?php echo $this->Form->input('type', array(
                            'label' => '',
                            'type' => 'select',
                            'options' => $types,
                            'selected' => $this->request->data['type']
                        )); ?>
                    </div>
                    <div class="input">
                        <?php echo $this->Form->input('location', array(
                            'label' => '',
                            'type' => 'select',
                            'options' => $locations,
                            'selected' => $this->request->data['location']
                        )); ?>
                    </div>
                    <div class="input">
                        <?php echo $this->Form->input('genre', array(
                            'label' => '',
                            'type' => 'select',
                            'options' => $genres,
                            'selected' => $this->request->data['genre']
                        )); ?>
                    </div>
                    <div class="clear"></div>
                    <div class="input">
                        <?php echo $this->Form->input('search', array(
                            'label' => '',
                            'type' => 'text',
                            'value' => $this->request->data['search']
                        )); ?>
                    </div>
                    <div class="input buttons">
                        <button type="submit" name="data[filter]" value="filter">Filter</button>
			<button type="submit" name="data[reset]" value="reset">Reset</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>