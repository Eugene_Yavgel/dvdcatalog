<ol>
    <?php
        // get recently added dvds
        $recently_added = $this->requestAction('dvds/footer/sort:created/direction:desc/limit:5');
        // loop through dvds
        foreach($recently_added as $dvd) {
            echo "<li><a href='/dvdcatalog/dvds/view/".$dvd['Dvd']['slug']."'>".$dvd['Dvd']['name']."</a></li>";
        }
    ?>
</ol>