<?php

class Dvd extends AppModel {
    
    // setup the belongs to relationships
    public $belongsTo = array(
        'Format'=>array(
            'className'=>'Format'
        ),
        'Type'=>array(
            'className'=>'Type'
        ),
        'Location'=>array(
            'className'=>'Location'
        )
    );

    // setup the has and belongs to many relationship
    public $hasAndBelongsToMany = array(
        'Genre'=>array(
            'className'=>'Genre'
        )
    );
    
    // setup form validation for dvd
    public $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'Please enter a Dvd Name'
        ),
        'format_id' => array(
            'rule' => 'numeric'
        ),
        'type_id' => array(
            'rule' => 'numeric'
        ),
        'location_id' => array(
            'rule' => 'numeric'
        )//        
    );
    
}

?>