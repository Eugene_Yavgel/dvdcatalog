-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 11 2015 г., 11:31
-- Версия сервера: 5.6.16
-- Версия PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `dvdcatalog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `last_login`, `status`) VALUES
(1, 'admin', '698d51a19d8a121ce581499d7b701668', '2015-11-22 16:36:05', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dvds`
--

CREATE TABLE IF NOT EXISTS `dvds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `format_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `imdb` varchar(255) NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `discs` tinyint(4) DEFAULT NULL,
  `episodes` tinyint(4) DEFAULT NULL,
  `views` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `dvds`
--

INSERT INTO `dvds` (`id`, `format_id`, `type_id`, `location_id`, `name`, `slug`, `image`, `website`, `imdb`, `rating`, `discs`, `episodes`, `views`, `created`, `modified`, `status`) VALUES
(1, 1, 1, 1, 'Desperado', 'desperado', 'img/dvds/17-11-2015-214715 - desperado.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0112851/', 8, 0, 0, 46, '2015-11-17 21:47:15', '2015-12-11 11:21:18', 1),
(2, 1, 1, 1, 'From Dusk Till Dawn', 'from_dusk_till_dawn', 'img/dvds/From_Dusk_Till_Dawn.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0116367/?ref_=nv_sr_6', 8, 0, 0, 2, '2015-11-18 21:21:05', '2015-11-25 04:17:10', 1),
(3, 1, 1, 1, 'Jackie Brown', 'jackie_brown', 'img/dvds/19-11-2015-003649 - Jackie_Brown.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0119396/?ref_=nv_sr_3', 6, 0, 0, 0, '2015-11-18 23:46:35', '2015-11-19 00:36:49', 1),
(4, 1, 1, 1, 'Pulp Fiction', 'pulp_fiction', 'img/dvds/18-11-2015-220017 - Pulp_Fiction.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0110912/?ref_=nv_sr_1', 9, NULL, NULL, 0, '2015-11-18 22:00:17', '2015-11-18 22:00:17', 1),
(5, 1, 1, 1, 'Kill Bill Vol.1', 'kill_bill_vol1', 'img/dvds/Kill_Bill_Vol.1.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0266697/?ref_=nv_sr_3', 7, NULL, NULL, 1, '2015-11-19 00:07:54', '2015-11-25 04:07:27', 1),
(6, 1, 1, 1, 'Kill Bill Vol.2', 'kill_bill_vol2', 'img/dvds/Kill_Bill_Vol.2.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0378194/?ref_=nv_sr_2', 8, NULL, NULL, 0, '2015-11-19 00:16:19', '2015-11-19 00:16:19', 1),
(7, 1, 1, 1, 'Reservoir dogs', 'reservoir_dogs', 'img/dvds/18-11-2015-223159 - Reservoir_Dogs.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0105236/?ref_=nv_sr_1', 8, NULL, NULL, 1, '2015-11-19 00:21:37', '2015-11-25 04:07:42', 1),
(8, 1, 1, 1, 'Four Rooms', 'four_rooms', 'img/dvds/Four_Rooms.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0113101/?ref_=nm_flmg_wr_17', 7, NULL, NULL, 2, '2015-11-19 00:28:42', '2015-11-25 02:07:21', 1),
(9, 1, 1, 1, 'Inglourious Basterds', 'inglourious_basterds', 'img/dvds/Inglourious_Basterds.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0361748/?ref_=nm_flmg_wr_6', 8, NULL, NULL, 0, '2015-11-25 01:10:05', '2015-11-25 01:10:05', 1),
(10, 1, 1, 1, 'Django Unchained', 'django_unchained', 'img/dvds/Django_Unchained.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt1853728/?ref_=nm_flmg_wr_5', 9, NULL, NULL, 4, '2015-11-25 01:15:00', '2015-11-25 04:12:12', 1),
(11, 2, 1, 1, 'Shawshenk redemption', 'shawshenk_redemption', 'img/dvds/Shawshank_redemption.jpg', 'http://www.kinogo.co', 'http://www.imdb.com/title/tt0111161/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239792642&pf_rd_r=1WJBTJ088QD8KEF3B8W3&pf_rd_s=center-1&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_tt_1', 10, NULL, NULL, 3, '2015-12-11 11:23:46', '2015-12-11 11:30:40', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dvds_genres`
--

CREATE TABLE IF NOT EXISTS `dvds_genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dvd_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `dvds_genres`
--

INSERT INTO `dvds_genres` (`id`, `dvd_id`, `genre_id`) VALUES
(5, 1, 1),
(2, 2, 1),
(3, 5, 1),
(4, 6, 1),
(6, 1, 2),
(7, 9, 2),
(8, 10, 2),
(12, 11, 4),
(11, 11, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `formats`
--

CREATE TABLE IF NOT EXISTS `formats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `descr` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `formats`
--

INSERT INTO `formats` (`id`, `name`, `slug`, `descr`, `created`, `modified`, `status`) VALUES
(1, 'DVD', 'dvd', 'An Original DVD.', '2015-11-13 00:00:00', '2015-11-27 03:37:12', 1),
(2, 'DVD Copy', 'dvd_copy', 'A Copied DVD.', '2015-11-13 04:05:34', '2015-11-13 04:13:23', 1),
(3, 'DVD Rip', 'dvd_rip', 'A Rip of a DVD.', '2015-11-13 04:18:46', '2015-11-13 11:03:50', 1),
(4, 'TV Rip', 'tv_rip', 'This has been recorded from a live TV broadcast.', '2015-11-13 11:13:45', '2015-11-13 11:17:26', 1),
(5, 'HD', 'hd', 'Full High Definition.', '2015-11-13 11:20:47', '2015-11-13 11:24:24', 1),
(6, 'HDTV', 'hdtv', 'High Definition but from a TV Source.', '2015-11-13 11:23:45', '2015-11-13 11:23:45', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `genres`
--

CREATE TABLE IF NOT EXISTS `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `genres`
--

INSERT INTO `genres` (`id`, `name`, `slug`, `created`, `modified`, `status`) VALUES
(1, 'Actionn', 'actionn', '2015-11-19 21:56:10', '2015-11-27 03:38:07', 1),
(2, 'Adventure', 'adventure', '2015-11-19 21:56:51', '2015-11-19 21:56:51', 1),
(3, 'Crime', 'crime', '2015-11-19 21:58:02', '2015-11-19 21:58:02', 1),
(4, 'Drama', 'drama', '2015-11-19 21:58:20', '2015-11-19 21:58:20', 1),
(5, 'Horror', 'horror', '2015-11-19 21:58:31', '2015-11-19 21:58:31', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `descr` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `locations`
--

INSERT INTO `locations` (`id`, `name`, `slug`, `descr`, `created`, `modified`, `status`) VALUES
(1, 'Shelf', 'shelf', 'Located on a Shelf.', '2015-11-15 15:13:45', '2015-11-19 00:45:24', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `layout_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `descr` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `types`
--

INSERT INTO `types` (`id`, `name`, `slug`, `descr`, `created`, `modified`, `status`) VALUES
(1, 'Film', 'film', 'This Item is a Film.', '2015-11-15 15:01:30', '2015-11-19 00:52:05', 1),
(2, 'TV Show', 'tv_show', 'This Item is a TV Show.', '2015-11-15 15:07:23', '2015-11-19 00:52:27', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
