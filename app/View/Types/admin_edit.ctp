<div class="types form">
    <?php echo $this->Form->create('Type'); ?>
        <fieldset>
            <legend>Edit Type</legend>
            <?php
                // a hidden form input field, required in a edit form
                echo $this->Form->input('id', array('type'=>'hidden'));
                // create the form inputs
                echo $this->Form->input('name', array('label'=>'Name:'));
                echo $this->Form->input('descr', array('label'=>'Description:', 'type'=>'textarea'));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Edit');?>
</div>

<?php if(!empty($this->data['Dvd'])): ?>

<div class="related">
    <h3>DVDs with this Type</h3>
    <table>
    	<thead>
            <tr>
            	<th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($this->data['Dvd'] as $dvd): ?>
            <tr>
            	<td><?php echo $dvd['id']; ?></td>
                <td><?php echo $dvd['name']; ?></td>
                <td><?php echo $this->Html->link('Edit', '/dvds/admin_edit/'.$dvd['id']);?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php endif; ?>

<ul class="actions">
    <li><?php echo $this->Html->link('List Types', array('action'=>'index'));?></li>
</ul>