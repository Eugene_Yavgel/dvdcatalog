<div class="dvds index">
    <h2>Dvds Admin Index</h2>
    <p>Currently displaying all DVDs in the application</p>

    <?php
	// check $dvds variable exists and is not empty
	if(isset($dvds) && !empty($dvds)) :
    ?>

    <table class="stripe">
    	<thead>
            <tr>
		<th>Name</th>
            	<th>Format</th>
                <th>Type</th>
                <th>Location</th>
		<th>Rating</th>
                <th>Created</th>
		<th>Modified</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($dvds as $dvd): ?>
            <tr>
                <td><?php echo h($dvd['Dvd']['name']); ?></td>
                <td><?php echo $this->Html->link($dvd['Format']['name'], array('controller'=> 'formats', 'action'=>'admin_edit', $dvd['Format']['id'])); ?></td>
                <td><?php echo $this->Html->link($dvd['Type']['name'], array('controller'=> 'types', 'action'=>'admin_edit', $dvd['Type']['id'])); ?></td>
                <td><?php echo $this->Html->link($dvd['Location']['name'], array('controller'=> 'locations', 'action'=>'admin_edit', $dvd['Location']['id'])); ?></td>
                <td><?php echo $dvd['Dvd']['rating']; ?></td>
                <td><?php echo $dvd['Dvd']['created']; ?></td>
                <td><?php echo $dvd['Dvd']['modified']; ?></td>
                <td>
                    <?php echo $this->Html->link('Edit', array('action'=>'admin_edit', $dvd['Dvd']['id']) );?> |
                    <?php echo $this->Html->link('Delete', array('action'=>'admin_delete', $dvd['Dvd']['id']), null, sprintf('Are you sure you want to delete Dvd: %s?', $dvd['Dvd']['name']));?>
		</td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?php
	else:
            echo 'There are currently no DVDs in the database.';
	endif;
    ?>
    
    <p><ul class="actions">
        <li><?php echo $this->Html->link('Add a DVD', array('action'=>'admin_add')); ?></li>
    </ul></p>
</div>