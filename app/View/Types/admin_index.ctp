<div class="types index">
    <h2>Types Admin Index</h2>
    <p>Currently displaying all types in the application.</p>

    <?php
	// check $types variable exists and is not empty
	if(isset($types) && !empty($types)) :
    ?>

    <table>
    	<thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Slug</th>
                <th>Description</th>
                <th>Created</th>
                <th>Modified</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                // initialise a counter for striping the table
                $count = 0;

                // loop through and display format
                foreach($types as $type):
                    // stripes the table by adding a class to every other row
                    $class = ( ($count % 2) ? " class='altrow'": '' );
                    // increment count
                    $count++;
            ?>
            <tr<?php echo $class; ?>>
            	<td><?php echo $type['Type']['id']; ?></td>
                <td><?php echo h($type['Type']['name']); ?></td>
                <td><?php echo h($type['Type']['slug']); ?></td>
                <td><?php echo h($type['Type']['descr']); ?></td>
                <td><?php echo $type['Type']['created']; ?></td>
                <td><?php echo $type['Type']['modified']; ?></td>
                <td>
                    <?php echo $this->Html->link('Edit', array('action'=>'admin_edit', $type['Type']['id']) );?> |
                    <?php echo $this->Html->link('Delete', array('action'=>'admin_delete', $type['Type']['id']), null, sprintf('Are you sure you want to delete Type: %s?', $type['Type']['name']));?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <?php
	else:
            echo 'There are currently no Types in the database.';
	endif;
    ?>    
    <p><ul class="actions">
        <li><?php echo $this->Html->link('Add a Type', array('action'=>'admin_add')); ?></li>
    </ul></p>
</div>