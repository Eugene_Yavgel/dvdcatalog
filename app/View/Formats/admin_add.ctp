<div class="formats form">
    <?php echo $this->Form->create('Format'); ?>
        <fieldset>
            <legend>Add a Format</legend>
            <?php
                // create the form inputs
                echo $this->Form->input('name', array('label'=>'Name:'));
                echo $this->Form->input('descr', array('label'=>'Description:', 'type'=>'textarea'));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Add');?>
</div>

<ul class="actions">
    <li><?php echo $this->Html->link('List Formats', array('action'=>'index'));?></li>
</ul>