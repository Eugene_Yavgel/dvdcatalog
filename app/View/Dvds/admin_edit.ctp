<div class="dvds form">
    <?php
        // if there was an error uploading the file then display errors here
        if(isset($errors)) {
            echo $this->misc->display_errors($errors);
        }
    ?>

    <?php echo $this->Form->create('Dvd', array('type'=>'file'));?>
        <fieldset>
            <legend>Edit a Dvd</legend>
            <?php
                // create the form inputs

                // include the id of the DVD as a form input
                // CakePHP will automatically create this as a hidden element
                echo $this->Form->input('id');
                echo $this->Form->input('name', array('label'=>'Name:'));
                echo $this->Form->input('format_id', array('label'=>'Format:', 'type'=>'select', 'options'=>$formats));
                echo $this->Form->input('type_id', array('label'=>'Type:', 'class'=>'type_select'));
                echo $this->Form->input('location_id', array('label'=>'Location:'));

                // display image if it exists
                if(!empty($this->request->data['Dvd']['image'])): 
            ?>
            <div class="input">
                <label>Current Image:</label>
                <img src="/dvdcatalog/app/webroot/<?php echo $this->request->data['Dvd']['image']; ?>" alt="Dvd Image" width="100" />
            </div>			
            <?php endif;

                echo $this->Form->input('File.image', array('label'=>'Image:', 'type'=>'file'));
                echo $this->Form->input('rating', array('label'=>'Rating:'));
                echo $this->Form->input('website', array('label'=>'Website URL:'));
                echo $this->Form->input('imdb', array('label'=>'Imdb URL:'));
                echo $this->Form->input('discs', array('label'=>'Number of Discs:', 'class'=>'tv_hide'));
                echo $this->Form->input('episodes', array('label'=>'Number of Episodes:', 'class'=>'tv_hide'));
                
                echo $this->Form->input('genres', array('label'=>'Genres:', 'type'=>'text', 'value'=>$genres_str));
            ?>
        </fieldset>
    <?php echo $this->Form->end('Save');?>
</div>

<ul class="actions">
    <p><li><?php echo $this->Html->link('List DVDs', array('action'=>'index'));?></li></p>
    <p><li><?php echo $this->Html->link('Add a DVD', array('action'=>'admin_add'));?></li></p>
</ul>